###################################################
#   Repair-Search - iOS Project 	#
##  Author: @RenatDz                               ##
###################################################

## Primeiros passos
Se você acabou de baixar o projeto, não se esqueça de executar o seguinte comando no terminal.
(Lembre-se também de estar no mesmo diretório do projeto)

### Instalando dependências
```
pod install
```

### Dúvidas
Dúvidas sobre o projeto, envie um email para: renatodz86@gmail.com
