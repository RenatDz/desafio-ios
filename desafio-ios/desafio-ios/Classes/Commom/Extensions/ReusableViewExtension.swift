//
//  ReusableViewExtension.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

extension ReusableView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    
}
