//
//  UIImageExtension.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

extension UIImage {
    // Init with view
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.init(cgImage: image!.cgImage!)
    }

}
