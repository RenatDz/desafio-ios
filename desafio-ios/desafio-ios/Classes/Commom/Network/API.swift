//
//  API.swift
//  desafio-ios
//
//  Created by Renato Mendes on 12/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import Alamofire

struct API {
    static let baseUrl: String = "https://api.github.com/"

    static let headers:   HTTPHeaders = [
        "Content-Type": "application/json"
    ]
}
