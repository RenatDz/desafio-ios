//
//  APIManager.swift
//  desafio-ios
//
//  Created by Renato Mendes on 12/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

struct APIManager: APIManageable {
    
    // MARK: - API Services
    
    func fetchRepositories(with language: String, and page: Page, complete: @escaping (_ repositories: [GHRepository]?) -> Void) {
        var url = Endpoints.github.fetchRepositories.url
        url += "?q=language:\(language)&sort=stars&page=\(page.curPage!)&per_page=\(page.perPage!)"
        
        let request = Alamofire.request(url, headers: API.headers)
        request.validate()
        request.responseObject { (response: DataResponse<GHItem>) in
            
            switch response.result {
            case .success( _):
                let repositories = response.result.value?.repositories
                complete(repositories)
                
            case .failure(let error):
                print(error.localizedDescription)
                complete(nil)
            }
        }
    }
    
    func fetchPulls(with midUrl: String, and page: Page, complete: @escaping (_ pulls: [GHPull]?) -> Void) {
        
        var url = Endpoints.github.fetchPulls.url
        url += "/\(midUrl)pulls?page=\(page.curPage!)&per_page=\(page.perPage!)"
        
        let request = Alamofire.request(url, headers: API.headers)
        request.validate()
        request.responseArray { (response: DataResponse<[GHPull]>) in
            
            switch response.result {
            case .success( _):
                let pulls = response.result.value
                complete(pulls)
                
            case .failure(let error):
                print(error.localizedDescription)
                complete(nil)
            }
        }
    }
    
}
