//
//  Endpoints.swift
//  desafio-ios
//
//  Created by Renato Mendes on 12/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

protocol Endpoint {
    var path: String { get }
    var url:  String { get }
}

enum Endpoints {
    
    enum github: Endpoint {
        case fetchRepositories
        case fetchPulls
        
        public var path: String {
            switch self {
            case .fetchRepositories: return "search/repositories"
            case .fetchPulls:        return "repos"
            }
        }
        
        public var url: String {
            return "\(API.baseUrl)\(path)"
        }
        
    }
    
}
