//
//  GHPull.swift
//  desafio-ios
//
//  Created by Renato Mendes on 12/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import ObjectMapper

struct GHPull: Mappable {
    
    var id:    Int!
    var url:   String!
    var title: String!
    var user:  GHOwner!
    var body:  String!
    var date:  String!
    
    init() {
        id    = nil
        url   = nil
        title = nil
        user  = GHOwner()
        body  = nil
        date  = nil
    }
    
    init?(map: Map) {
        mapping(map: map)
    }
    
    mutating func mapping(map: Map) {
        id    <- map["id"]
        url   <- map["html_url"]
        title <- map["title"]
        user  <- map["user"]
        body  <- map["body"]
        date  <- map["created_at"]
    }
    
}
