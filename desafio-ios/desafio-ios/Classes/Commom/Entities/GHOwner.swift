//
//  GHOwner.swift
//  desafio-ios
//
//  Created by Renato Mendes on 12/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import ObjectMapper

struct GHOwner: Mappable {
    
    var id                : Int!
    var login             : String!
    var avatarUrl         : String!
    var gravatarId        : String!
    var url               : String!
    var htmlUrl           : String!
    var followersUrl      : String!
    var followingUrl      : String!
    var gistsUrl          : String!
    var starredUrl        : String!
    var subscriptionsUrl  : String!
    var organizationsUrl  : String!
    var reposUrl          : String!
    var eventsUrl         : String!
    var receivedEventsUrl : String!
    var type              : String!
    var siteAdmin         : Bool!
    var name              : String!
    var company           : String!
    var blog              : String!
    var location          : String!
    var email             : String!
    var hireable          : String!
    var bio               : String!
    var publicRepos       : Int!
    var publicGists       : Int!
    var followers         : Int!
    var following         : Int!
    var createdAt         : String!
    var updatedAt         : String!
    
    init() {
        id                = nil
        login             = nil
        avatarUrl         = nil
        gravatarId        = nil
        url               = nil
        htmlUrl           = nil
        followersUrl      = nil
        followingUrl      = nil
        gistsUrl          = nil
        starredUrl        = nil
        subscriptionsUrl  = nil
        organizationsUrl  = nil
        reposUrl          = nil
        eventsUrl         = nil
        receivedEventsUrl = nil
        type              = nil
        siteAdmin         = nil
        name              = nil
        company           = nil
        blog              = nil
        location          = nil
        email             = nil
        hireable          = nil
        bio               = nil
        publicRepos       = nil
        publicGists       = nil
        followers         = nil
        following         = nil
        createdAt         = nil
        updatedAt         = nil
    }
    
    init?(map: Map){
        mapping(map: map)
    }
    
    mutating func mapping(map: Map) {
        id                <- map["id"]
        login             <- map["login"]
        avatarUrl         <- map["avatar_url"]
        gravatarId        <- map["gravatar_id"]
        url               <- map["url"]
        htmlUrl           <- map["html_url"]
        followersUrl      <- map["followers_url"]
        followingUrl      <- map["following_url"]
        gistsUrl          <- map["gists_url"]
        starredUrl        <- map["starred_url"]
        subscriptionsUrl  <- map["subscriptions_url"]
        organizationsUrl  <- map["organizations_url"]
        reposUrl          <- map["repos_url"]
        eventsUrl         <- map["events_url"]
        receivedEventsUrl <- map["received_events_url"]
        type              <- map["type"]
        siteAdmin         <- map["site_admin"]
        name              <- map["name"]
        company           <- map["company"]
        blog              <- map["blog"]
        location          <- map["location"]
        email             <- map["email"]
        hireable          <- map["hireable"]
        bio               <- map["bio"]
        publicRepos       <- map["public_repos"]
        publicGists       <- map["public_gists"]
        followers         <- map["followers"]
        following         <- map["following"]
        createdAt         <- map["created_at"]
        updatedAt         <- map["updated_at"]
    }
    
}
