//
//  GHItem.swift
//  desafio-ios
//
//  Created by Renato Mendes on 12/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import ObjectMapper

struct GHItem: Mappable {
    
    var totalCount        : Int!
    var incompleteResults : Bool!
    var repositories      : [GHRepository]!
    
    init() {
        totalCount        = nil
        incompleteResults = nil
        repositories      = [GHRepository]()
    }
    
    init?(map: Map){
        mapping(map: map)
    }
    
    mutating func mapping(map: Map) {
        totalCount        <- map["total_count"]
        incompleteResults <- map["incomplete_results"]
        repositories      <- map["items"]
    }
    
}
