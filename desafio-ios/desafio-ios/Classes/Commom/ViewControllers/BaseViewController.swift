//
//  BaseViewController.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit
import SWRevealViewController

class BaseViewController: UIViewController {
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension BaseViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        navigationController.navigationBar.barStyle                 = .black
        navigationController.navigationBar.barTintColor             = NAV_COLOR
        navigationController.navigationBar.tintColor                = WHITE_COLOR
        navigationController.navigationBar.titleTextAttributes      = NAV_TEXT_ATT
        navigationController.navigationBar.isTranslucent            = false
        navigationController.navigationBar.shadowImage              = UIImage()
        
        if #available(iOS 11.0, *) {
            navigationController.navigationBar.largeTitleTextAttributes = NAV_TEXT_ATT
            navigationController.navigationBar.prefersLargeTitles       = true
        }
        
        guard let revealControl = viewController.revealViewController() else  { return }
        
        switch viewController {
        case _ as RepositoriesViewController:
            let btMenu = UIBarButtonItem(image: ICN_NAV_MENU, style: .plain, target: revealControl, action: #selector(SWRevealViewController.revealToggle(_:)))
            viewController.navigationItem.setLeftBarButton(btMenu, animated: true)
            
        default:
            break
        }
    }
    
}
