//
//  RepositoryType.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

enum RepositoryType: String {
    case java   = "Java"
    case swift  = "Swift"
    case ruby   = "Ruby"
    case objc   = "Objective-C"
    case php    = "PHP"
    case python = "Python"
}
