//
//  ViewProtocols.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import Foundation

protocol ReusableView: class {}
protocol NibLoadableView: class {}
