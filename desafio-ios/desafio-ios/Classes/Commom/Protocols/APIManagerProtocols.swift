//
//  APIManagerProtocols.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

protocol APIManageable {
    func fetchRepositories(with language: String, and page: Page, complete: @escaping (_ repositories: [GHRepository]?) -> Void)
    
    func fetchPulls(with midUrl: String, and page: Page, complete: @escaping (_ pulls: [GHPull]?) -> Void)
}
