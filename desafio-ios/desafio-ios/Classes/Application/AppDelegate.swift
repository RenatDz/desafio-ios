//
//  AppDelegate.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit
import AlamofireNetworkActivityIndicator

@UIApplicationMain
class AppDelegate: UIResponder {

    // MARK: - Properties
    
    var window: UIWindow?
    
    // MARK: - Setups
    
    func setupActivityNetwork() {
        NetworkActivityIndicatorManager.shared.isEnabled       = true
        NetworkActivityIndicatorManager.shared.startDelay      = 0.2
        NetworkActivityIndicatorManager.shared.completionDelay = 0.2
    }
    
    func setupNavBar() {
        UINavigationBar.appearance().backIndicatorImage = ICN_NAV_BACK
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = ICN_NAV_BACK
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-160, 0), for: .default)
    }

}

extension AppDelegate: UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setupActivityNetwork()
        
        setupNavBar()
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        window?.frame = UIScreen.main.bounds
        
        RootRouter.assembleModule(with: window!)
        
        return true
    }
    
}
