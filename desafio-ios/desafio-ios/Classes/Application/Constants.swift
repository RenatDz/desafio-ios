//
//  Constants.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

// MARK: - Assets

let ICN_NAV_MENU = UIImage(named: "icn_nav_menu")
let ICN_NAV_BACK = UIImage(named: "icn_nav_back")

let ICN_CELL_FORK = UIImage(named: "icn_cell_fork")
let ICN_CELL_STAR = UIImage(named: "icn_cell_star")

// MARK: - Colors

let NAV_COLOR    = UIColor(hex: "212121")
let NAV_TEXT_ATT = [NSAttributedStringKey.foregroundColor : WHITE_COLOR]
let WHITE_COLOR  = UIColor(hex: "FFFFFF")
