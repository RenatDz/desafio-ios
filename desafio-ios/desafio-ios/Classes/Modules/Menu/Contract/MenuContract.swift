//
//  MenuContract.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

protocol MenuInterface: class {
    var presenter: MenuPresentation! { get set }
    var itens: [RepositoryType]!     { get set }
}

protocol MenuPresentation: class {
    var router: MenuWireframe! { get set }
    
    func didSelect(item: RepositoryType)
}

protocol MenuWireframe: class {
    weak var view: UIViewController? { get set }
    
    static func assembleModule() -> UIViewController
    func presentRepositoriesModule(with type: RepositoryType)
}
