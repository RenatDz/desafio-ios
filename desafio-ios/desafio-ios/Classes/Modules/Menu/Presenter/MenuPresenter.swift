//
//  MenuPresenter.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

class MenuPresenter {
    
    var router: MenuWireframe!
    
}

extension MenuPresenter: MenuPresentation {
    
    func didSelect(item: RepositoryType) {
        router.presentRepositoriesModule(with: item)
    }
    
}
