//
//  MenuViewController.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, MenuInterface {

    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var presenter: MenuPresentation!
    var itens: [RepositoryType]! = [.java, .swift, .ruby, .objc, .php, .python]
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itens.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Languages"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as MenuCell
        cell.setup(with: itens[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelect(item: itens[indexPath.row])
    }
    
}
