//
//  MenuCell.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var title: UILabel!
    
    // MARK: - Lifecyle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: - Setup
    
    func setup(with item: RepositoryType) {
        title.text = item.rawValue
    }

}
