//
//  MenuRouter.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit
import SWRevealViewController

class MenuRouter {
    
    // MARK: - Properties
    
    weak var view: UIViewController?
    
}

extension MenuRouter: MenuWireframe {
    
    static func assembleModule() -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let view       = storyboard.instantiateViewController(MenuViewController.self)
        
        let presenter = MenuPresenter()
        let router    = MenuRouter()
        
        presenter.router = router
        view.presenter   = presenter
        
        router.view = view
        
        return view
    }
    
    func presentRepositoriesModule(with type: RepositoryType) {
        let revealController   = view?.revealViewController()
        let repositoriesModule = RepositoriesRouter.assembleModule(with: type)
        revealController?.pushFrontViewController(repositoriesModule, animated: true)
    }
    
}
