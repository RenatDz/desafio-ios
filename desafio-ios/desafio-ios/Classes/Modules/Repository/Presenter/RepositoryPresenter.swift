//
//  RepositoryPresenter.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

class RepositoryPresenter {
    
    // MARK: - Properties
    
    weak var interface: RepositoryInterface?
    var interactor: RepositoryUseCase!
    var router: RepositoryWireframe!
    var midUrl: String!
    var page: Page! = Page(curPage: 1, perPage: 10)
    var pulls: [GHPull]! = [] {
        didSet {
            if pulls.count > 0 {
                interface?.showPullsContent(pulls)
            } else {
                interface?.showNoContent()
            }
        }
    }
}

extension RepositoryPresenter: RepositoryPresentation {
    
    func viewDidLoad() {
        interactor.fetchPulls(with: midUrl, and: page)
    }
    
    func loadMoreContent() {
        interactor.fetchPulls(with: midUrl, and: page)
    }
    
    func didSelect(_ pull: GHPull) {
        router.presentPullDetails(with: pull)
    }
    
    func didTouchOnTryAgain() {
        page = Page(curPage: 1, perPage: 10)
        interactor.fetchPulls(with: midUrl, and: page)
    }
    
}

extension RepositoryPresenter: RepositoryInteractorOutput {
    
    func pullsWasFetched(_ pulls: [GHPull]) {
        page.curPage = page.curPage + 1
        
        self.pulls = pulls
    }
    
    func fetchedWasFailed() {
        interface?.showNoContent()
    }
    
}
