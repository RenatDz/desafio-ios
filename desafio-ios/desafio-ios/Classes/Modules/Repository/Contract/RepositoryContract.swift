//
//  RepositoryContract.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

protocol RepositoryInterface: class {
    var presenter: RepositoryPresentation! { get set }
    var repository: GHRepository!          { get set }
    var pulls: [GHPull]!                   { get set }
    
    func showPullsContent(_ pulls: [GHPull])
    func showNoContent()
}

protocol RepositoryPresentation: class {
    weak var interface: RepositoryInterface? { get set }
    var interactor: RepositoryUseCase!       { get set }
    var router: RepositoryWireframe!         { get set }
    var midUrl: String!                      { get set }
    var page: Page!                          { get set }
    var pulls: [GHPull]!                     { get set }
    
    func viewDidLoad()
    func loadMoreContent()
    func didSelect(_ pull: GHPull)
    func didTouchOnTryAgain()
}

protocol RepositoryUseCase: class {
    weak var output: RepositoryInteractorOutput? { get set }
    
    func fetchPulls(with midUrl: String, and page: Page)
}

protocol RepositoryInteractorOutput: class {
    func pullsWasFetched(_ pulls: [GHPull])
    func fetchedWasFailed()
}

protocol RepositoryWireframe: class {
    weak var view: UIViewController? { get set }
    
    static func assembleModule(with repository: GHRepository) -> UIViewController
    func presentPullDetails(with pull: GHPull)
}
