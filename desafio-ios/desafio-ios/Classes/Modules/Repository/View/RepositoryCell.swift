//
//  RepositoryCell.swift
//  desafio-ios
//
//  Created by Renato Mendes on 12/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryCell: UICollectionViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var pullRequestTitle: UILabel!
    @IBOutlet weak var pullRequestDescription: UILabel!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userUsername: UILabel!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pullRequestTitle.text       = ""
        pullRequestDescription.text = ""
    }
    
    // MARK: - Setups
    
    func setup(with pull: GHPull) {
        pullRequestTitle.text       = pull.title
        pullRequestDescription.text = pull.body
        
        userImage.sd_setImage(with: URL(string: pull.user.avatarUrl))
        userUsername.text = pull.user.login
    }

}
