//
//  RepositoryHeader.swift
//  desafio-ios
//
//  Created by Renato Mendes on 12/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

class RepositoryHeader: UICollectionViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var forksCount: UILabel!
    @IBOutlet weak var starsCount: UILabel!
    @IBOutlet weak var watchersCount: UILabel!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Setup
    
    func setup(with repository: GHRepository) {
        forksCount.text    = "\(repository.forksCount!)"
        starsCount.text    = "\(repository.stargazersCount!)"
        watchersCount.text = "\(repository.watchersCount!)"
    }

}
