//
//  RepositoryViewController.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

class RepositoryViewController: BaseViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noContentView: UIView!
    
    // MARK: - Constants
    
    let loading: UIActivityIndicatorView! = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    // MARK: - Properties
    
    var presenter: RepositoryPresentation!
    var repository: GHRepository!
    var pulls: [GHPull]! = []
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "\(repository.name!)"
        
        setupCollectionView()
        
        presenter.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            if #available(iOS 9.0, *) {
                flowLayout.sectionHeadersPinToVisibleBounds = true
            }
            flowLayout.itemSize = CGSize(width: view.frame.width, height: 126)
            flowLayout.invalidateLayout()
        }
    }
    
    // MARK: - Setups
    
    func setupCollectionView() {
        collectionView.addInfiniteScroll { _ in
            self.presenter.loadMoreContent()
        }
        
        collectionView.setShouldShowInfiniteScrollHandler { _ in
            return self.presenter.page.curPage < 10
        }
        
        // Start loading
        loading.center = self.view.center
        self.view.addSubview(loading)
        loading.startAnimating()
    }
    
    // MARK: - Actions
    
    @IBAction func didTouchOnTryAgain(_ sender: Any) {
        self.view.sendSubview(toBack: noContentView)
        self.view.addSubview(loading)
        loading.startAnimating()
        presenter.didTouchOnTryAgain()
    }
    
}

extension RepositoryViewController: RepositoryInterface {
    
    func showPullsContent(_ pulls: [GHPull]) {
        let (start, end) = (self.pulls.count, pulls.count + self.pulls.count)
        let indexPaths = (start..<end).map { return IndexPath(row: $0, section: 0) }
        
        self.pulls.append(contentsOf: pulls)
        
        collectionView.performBatchUpdates({
            collectionView.insertItems(at: indexPaths)
            
            self.loading.stopAnimating()
            self.loading.removeFromSuperview()
        }, completion: { _ in
            self.collectionView.finishInfiniteScroll()
        })
    }
    
    func showNoContent() {
        collectionView.finishInfiniteScroll()
        
        if pulls.count == 0 {
            self.view.bringSubview(toFront: noContentView)
        }
    }
    
}

extension RepositoryViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pulls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath) as RepositoryCell
        
        cell.setup(with: pulls[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var header = RepositoryHeader()
        
        if kind == UICollectionElementKindSectionHeader {
            header = collectionView.dequeueReusableView(of: kind, for: indexPath) as RepositoryHeader
            header.setup(with: repository)
        }
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.didSelect(pulls[indexPath.item])
    }
    
}
