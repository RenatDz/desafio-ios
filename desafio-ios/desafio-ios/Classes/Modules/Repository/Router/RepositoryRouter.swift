//
//  RepositoryRouter.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit
import SafariServices

class RepositoryRouter: NSObject {
    
    // MARK: - Properties
    
    weak var view: UIViewController?
    
}

extension RepositoryRouter: RepositoryWireframe {
    
    static func assembleModule(with repository: GHRepository) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let view       = storyboard.instantiateViewController(RepositoryViewController.self)
        
        let presenter  = RepositoryPresenter()
        let interactor = RepositoryInteractor()
        let router     = RepositoryRouter()
        
        presenter.interactor = interactor
        presenter.router     = router
        presenter.interface  = view
        presenter.midUrl     = "\(repository.owner.login!)/\(repository.name!)/"
        
        interactor.output = presenter
        
        view.presenter  = presenter
        view.repository = repository
        
        router.view = view
        
        return view
    }
    
    func presentPullDetails(with pull: GHPull) {
        if let url = URL(string: pull.url) {
            if #available(iOS 9.0, *) {
                let svc = SFSafariViewController(url: url)
                view?.present(svc, animated: true, completion: nil)
            } else {
                guard UIApplication.shared.openURL(url) else {
                    print("Failed to open url: \(url.description)"); return
                }
            }
        }
    }
    
}

@available(iOS 9.0, *)
extension RepositoryRouter: SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        view?.dismiss(animated: true, completion: nil)
    }
    
}
