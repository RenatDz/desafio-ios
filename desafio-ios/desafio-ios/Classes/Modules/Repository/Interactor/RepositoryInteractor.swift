//
//  RepositoryInteractor.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

class RepositoryInteractor {
    
    // MARK: - Properties
    
    weak var output: RepositoryInteractorOutput?
    var apiManager: APIManager!
    
    init() {
        apiManager = APIManager()
    }
    
}

extension RepositoryInteractor: RepositoryUseCase {
    
    func fetchPulls(with midUrl: String, and page: Page) {
        apiManager.fetchPulls(with: midUrl, and: page) {
            pulls in
            
            guard pulls != nil else {
                self.output?.fetchedWasFailed(); return
            }
            
            self.output?.pullsWasFetched(pulls!)
        }
    }
    
}

