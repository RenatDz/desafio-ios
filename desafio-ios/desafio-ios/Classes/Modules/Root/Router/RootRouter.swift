//
//  RootRouter.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

class RootRouter {
    
}

extension RootRouter: RootWireframe {
    
    static func assembleModule(with window: UIWindow) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let view       = storyboard.instantiateViewController(RootViewController.self)
        
        let menuModule         = MenuRouter.assembleModule()
        let repositoriesModule = RepositoriesRouter.assembleModule(with: .java)
        
        // Set the menu
        view.setRear(menuModule, animated: true)
        
        // Set the first view which menu will load
        view.setFront(repositoriesModule, animated: true)

        window.rootViewController = view
    }
    
}
