//
//  RepositoriesPresenter.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

class RepositoriesPresenter {
    
    // MARK: - Properties
    
    weak var interface: RepositoriesInterface?
    var interactor: RepositoriesUseCase!
    var router: RepositoriesWireframe!
    var page: Page! = Page(curPage: 1, perPage: 10)
    var repositories: [GHRepository]! = [] {
        didSet {
            if repositories.count > 0 {
                interface?.showRepositoriesContent(repositories)
            } else {
                interface?.showNoContent()
            }
        }
    }
    
}

extension RepositoriesPresenter: RepositoriesPresentation {
 
    func viewDidLoad(with language: String) {
        interactor.fetchRepositories(with: language, and: page)
    }
    
    func loadMoreContent(with language: String) {
        interactor.fetchRepositories(with: language, and: page)
    }
    
    func didSelect(_ repository: GHRepository) {
        router.presentRepositoryModule(with: repository)
    }
    
    func didTouchOnTryAgain(with language: String) {
        page = Page(curPage: 1, perPage: 10)
        interactor.fetchRepositories(with: language, and: page)
    }
    
}

extension RepositoriesPresenter: RepositoriesInteractorOutput {
    
    func repositoriesWasFetched(_ repositories: [GHRepository]) {
        page.curPage = page.curPage + 1
        
        self.repositories = repositories
    }
    
    func fetchedWasFailed() {
        interface?.showNoContent()
    }
    
}
