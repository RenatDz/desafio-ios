//
//  RepositoriesInteractor.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

class RepositoriesInteractor {
 
    // MARK: - Properties
    
    weak var output: RepositoriesInteractorOutput?
    var apiManager: APIManager!
    
    init() {
        apiManager = APIManager()
    }
    
}

extension RepositoriesInteractor: RepositoriesUseCase {
    
    func fetchRepositories(with language: String, and page: Page) {
        apiManager.fetchRepositories(with: language, and: page) {
            repositories in
            
            guard repositories != nil else {
                self.output?.fetchedWasFailed(); return
            }
            
            self.output?.repositoriesWasFetched(repositories!)
        }
    }
    
}
