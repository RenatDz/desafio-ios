//
//  RepositoriesCell.swift
//  desafio-ios
//
//  Created by Renato Mendes on 12/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoriesCell: UICollectionViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var repositoryName: UILabel!
    @IBOutlet weak var repositoryDescription: UILabel!
    
    @IBOutlet weak var ownerImage: UIImageView!
    @IBOutlet weak var ownerUsername: UILabel!
    
    @IBOutlet weak var forkIcon: UIImageView!
    @IBOutlet weak var forksCount: UILabel!
    
    @IBOutlet weak var starsIcon: UIImageView!
    @IBOutlet weak var starsCount: UILabel!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        repositoryName.text        = ""
        repositoryDescription.text = ""
        
        ownerUsername.text = ""
        
        forkIcon.image  = UIImage()
        forksCount.text = ""
        
        starsIcon.image  = UIImage()
        starsCount.text = ""
    }
    
    // MARK: - Setups
    
    func setup(with repository: GHRepository) {
        repositoryName.text        = repository.name
        repositoryDescription.text = repository.Description
        
        ownerImage.sd_setImage(with: URL(string: repository.owner.avatarUrl))
        ownerUsername.text = repository.owner.login
        
        forkIcon.image  = ICN_CELL_FORK
        forksCount.text = "\(repository.forksCount!)"
        
        starsIcon.image = ICN_CELL_STAR
        starsCount.text = "\(repository.stargazersCount!)"
    }
    
    

}
