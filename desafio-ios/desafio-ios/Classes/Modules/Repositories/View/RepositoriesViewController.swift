//
//  RepositoriesViewController.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

class RepositoriesViewController: BaseViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noContentView: UIView!
    
    // MARK: - Properties
    
    var presenter: RepositoriesPresentation!
    var type: RepositoryType!
    var repositories: [GHRepository]! = []
    var isInitLoading: Bool!          = true
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "\(type.rawValue) Populars"
        
        setupCollectionView()
        
        presenter.viewDidLoad(with: type.rawValue)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if isInitLoading {
            Loader.addLoaderToCollectionView(collectionView)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.itemSize = CGSize(width: view.frame.width, height: 118)
            flowLayout.invalidateLayout()
        }
    }
    
    // MARK: - Setups
    
    func setupCollectionView() {
        collectionView.addInfiniteScroll { _ in
            self.presenter.loadMoreContent(with: self.type.rawValue)
        }
        
        collectionView.setShouldShowInfiniteScrollHandler { _ in
            return self.presenter.page.curPage < 10
        }
    }
    
    // MARK: - Actions
    
    @IBAction func didTouchOnTryAgain(_ sender: Any) {
        self.view.sendSubview(toBack: noContentView)
        presenter.didTouchOnTryAgain(with: self.type.rawValue)
    }
    
}

extension RepositoriesViewController: RepositoriesInterface {
    
    func showRepositoriesContent(_ repositories: [GHRepository]) {
        let (start, end) = (self.repositories.count, repositories.count + self.repositories.count)
        let indexPaths = (start..<end).map { return IndexPath(row: $0, section: 0) }
        
        self.repositories.append(contentsOf: repositories)
        
        collectionView.performBatchUpdates({
            if isInitLoading {
                let indexPaths = (0..<10).map { return IndexPath(row: $0, section: 0) }
                collectionView.deleteItems(at: indexPaths)
                
                isInitLoading = false
                Loader.removeLoaderFromCollectionView(collectionView)
            }
            
            collectionView.insertItems(at: indexPaths)
        }, completion: { _ in
            self.collectionView.finishInfiniteScroll()
        })
    }
    
    func showNoContent() {
        collectionView.finishInfiniteScroll()
        
        if repositories.count == 0 {
            self.view.bringSubview(toFront: noContentView); return
        }
    }
    
}

extension RepositoriesViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard !isInitLoading else {
            return 10
        }
        
        return repositories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath) as RepositoriesCell
        
        guard !isInitLoading else {
            return cell
        }
        
        cell.setup(with: repositories[indexPath.item])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard !isInitLoading else {
            return
        }
        
        presenter.didSelect(repositories[indexPath.item])
    }
    
}
