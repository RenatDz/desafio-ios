//
//  RepositoriesContract.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

protocol RepositoriesInterface: class {
    var presenter: RepositoriesPresentation! { get set }
    var type: RepositoryType!                { get set }
    
    func showRepositoriesContent(_ repositories: [GHRepository])
    func showNoContent()
}

protocol RepositoriesPresentation: class {
    weak var interface: RepositoriesInterface? { get set }
    var interactor: RepositoriesUseCase!       { get set }
    var router: RepositoriesWireframe!         { get set }
    var page: Page!                            { get set }
    var repositories: [GHRepository]!          { get set }
    
    func viewDidLoad(with language: String)
    func loadMoreContent(with language: String)
    func didSelect(_ repository: GHRepository)
    func didTouchOnTryAgain(with language: String)
}

protocol RepositoriesUseCase: class {
    weak var output: RepositoriesInteractorOutput? { get set }
    var apiManager: APIManager!                    { get set }
    
    func fetchRepositories(with language: String, and page: Page)
}

protocol RepositoriesInteractorOutput: class {
    func repositoriesWasFetched(_ repositories: [GHRepository])
    func fetchedWasFailed()
}

protocol RepositoriesWireframe: class {
    weak var view: UIViewController? { get set }
    
    static func assembleModule(with type: RepositoryType) -> UIViewController
    func presentRepositoryModule(with repository: GHRepository)
}
