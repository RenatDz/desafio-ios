//
//  RepositoriesRouter.swift
//  desafio-ios
//
//  Created by Renato Mendes on 11/11/17.
//  Copyright © 2017 Renato Mendes. All rights reserved.
//

import UIKit

class RepositoriesRouter {
    
    // MARK: - Properties
    
    weak var view: UIViewController?
    
}

extension RepositoriesRouter: RepositoriesWireframe {
    
    static func assembleModule(with type: RepositoryType) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let view       = storyboard.instantiateViewController(RepositoriesViewController.self)
        
        let presenter  = RepositoriesPresenter()
        let interactor = RepositoriesInteractor()
        let router     = RepositoriesRouter()
        
        presenter.interactor = interactor
        presenter.router     = router
        presenter.interface  = view
        
        interactor.output = presenter
        
        view.presenter = presenter
        view.type      = type
        
        router.view = view
        
        let nav = UINavigationController(rootViewController: view)
        
        return nav
    }
    
    func presentRepositoryModule(with repository: GHRepository) {
        let repositoryModule = RepositoryRouter.assembleModule(with: repository)
        view?.navigationController?.pushViewController(repositoryModule, animated: true)
    }
    
}
